<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class  WC_Gateway_Rabo_Omnikassa_Vpay extends WC_Gateway_Rabo_Omnikassa {

	public $id = 'raboomnikassa_vpay';
	public $paymentBrand = 'V_PAY';

	public function init_form_fields() {
		$this->form_fields = array(
			'enabled'              => array(
				'title'   => __( 'Enable/Disable', 'raboomnikassa' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Rabo OmniKassa V PAY', 'raboomnikassa' ),
				'default' => 'yes'
			),
			'title'                => array(
				'title'       => __( 'Title', 'raboomnikassa' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'raboomnikassa' ),
				'default'     => __( 'V PAY', 'raboomnikassa' ),
				'desc_tip'    => true,
			),
			'description'          => array(
				'title'       => __( 'Description', 'raboomnikassa' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'raboomnikassa' ),
				'default'     => __( 'V PAY', 'raboomnikassa' ),
				'desc_tip'    => true,
			),
		);
	}
}