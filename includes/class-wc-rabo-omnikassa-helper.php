<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_Rabo_Omnikassa_Helper {

	/**
	 * Check if WC version is pre 3.0.
	 */
	public static function is_pre_30() {
		return version_compare( WC_VERSION, '3.0.0', '<' );
	}

	/**
	 * Get Rabo OmniKassa general options.
	 *
	 * @var array $main_settings
	 * @var array $form_fields
	 *
	 * @return array
	 */
	public static function get_raboomnikassa_general_options() {
		$main_settings = get_option('woocommerce_raboomnikassa_settings', null);
		if (!$main_settings) {
			$form_fields   = WC_Rabo_Omnikassa_Helper::get_raboomnikassa_general_form_fields();
			$main_settings = array_merge( array_fill_keys( array_keys( $form_fields ), '' ), wp_list_pluck( $form_fields, 'default' ) );
		}
		return $main_settings;
	}

	public static function  get_raboomnikassa_general_form_fields() {
		return array(
			'enabled'                  => array(
				'title'   => __('Enable/Disable', 'raboomnikassa'),
				'type'    => 'checkbox',
				'label'   => __('PAYTIUM_ENABLE_WOOCOMMERCE_PAYMENT', 'raboomnikassa'),
				'default' => 'yes'
			),
			'version'                  => array(
				'title'       => __('Rabo OmniKassa Version ', 'raboomnikassa'),
				'type'        => 'select',
				'default'     => get_option('woocommerce_raboomnikassa_new_install') == true ? 2 : 1,
				'class'       => 'chosen_select',
				'options'     => array(
					'1' => 'Rabo Omnikassa 1.0',
					'2' => 'Rabo Omnikassa 2.0',
				),
			),
			'title'                    => array(
				'title'       => __('Title', 'raboomnikassa'),
				'type'        => 'text',
				'description' => __('This controls the title which the user sees during checkout.', 'raboomnikassa'),
				'default'     =>  get_option('woocommerce_raboomnikassa_new_install') == true ?  __('iDEAL, Credit Cards, Bancontact by Rabo OmniKassa', 'raboomnikassa') : __('PAYTIUM_DEFAULT_WOOCOMMERCE_PAYMENT', 'raboomnikassa'),
				'desc_tip'    => true,
			),
			'description'              => array(
				'title'       => __('Description', 'raboomnikassa'),
				'type'        => 'textarea',
				'description' => __('Payment method description that the customer will see on your checkout.', 'raboomnikassa'),
				'default'     => get_option('woocommerce_raboomnikassa_new_install') == true ? __('Use iDEAL, Credit Cards, Bancontact, PayPal and other payment methods with Rabo OmniKassa. The Rabo OmniKassa is an extended online cash register by the Rabobank, which we use to manage payments on our website.', 'raboomnikassa') : __('PAYTIUM_DEFAULT_WOOCOMMERCE_DESCRIPTION', 'raboomnikassa'),
				'desc_tip'    => true,
			),
			'status_section'           => array(
				'title'       => __('Status', 'raboomnikassa'),
				'type'        => 'title',
				'description' => __('When status is set to "test", you <strong>do not</strong> need to enter the test account details in "Live account details" below!', 'raboomnikassa'),
			),
			'status'                   => array(
				'title'       => __('PAYTIUM_RABO_OMNIKASSA_SERVER_LABEL', 'raboomnikassa'),
				'type'        => 'select',
				'description' => __('PAYTIUM_RABO_OMNIKASSA_SERVER_DESC', 'raboomnikassa'),
				'default'     => 'test',
				'desc_tip'    => true,
				'class'       => 'chosen_select',
				'options'     => array(
					'live' => __('PAYTIUM_OPTION_LIVE_SERVER', 'raboomnikassa'),
					'test' => __('PAYTIUM_OPTION_TEST_SERVER', 'raboomnikassa'),
				),
			),
			'account_details'          => array(
				'title' => __('Live account details', 'raboomnikassa'),
				'type'  => 'title',
			),
			'merchantid'               => array(
				'title'       => __('PAYTIUM_RABO_OMNIKASSA_MERCHANT_ID_LABEL', 'raboomnikassa'),
				'type'        => 'text',
				'description' => __('PAYTIUM_RABO_OMNIKASSA_MERCHANT_ID_DESC', 'raboomnikassa'),
				'default'     => '',
				'desc_tip'    => true,
			),
			'privatekeypass'           => array(
				'title'       => __('PAYTIUM_RABO_OMNIKASSA_PRIV_PASS_LABEL', 'raboomnikassa'),
				'type'        => 'text',
				'description' => __('PAYTIUM_RABO_OMNIKASSA_PRIV_PASS_DESC', 'raboomnikassa'),
				'default'     => '',
				'desc_tip'    => true,
			),
			'keyversion'               => array(
				'title'       => __('PAYTIUM_RABO_OMNIKASSA_KEYVERSION_LABEL', 'raboomnikassa'),
				'type'        => 'text',
				'description' => __('PAYTIUM_RABO_OMNIKASSA_KEYVERSION_DESC', 'raboomnikassa'),
				'default'     => 1,
				'desc_tip'    => true,
			),
			'production_details'       => array(
				'title' => __('Production details', 'raboomnikassa'),
				'type'  => 'title',
			),
			'production_refresh_token' => array(
				'title'       => __('Refresh token', 'raboomnikassa'),
				'type'        => 'text',
				'default'     => '',
			),
			'production_signing_key'   => array(
				'title'       => __('Signing key', 'raboomnikassa'),
				'type'        => 'text',
				'default'     => '',
			),
			'sandbox_details'          => array(
				'title' => __('Sandbox/Test details', 'raboomnikassa'),
				'type'  => 'title',
			),
			'sandbox_refresh_token'    => array(
				'title'       => __('Refresh token', 'raboomnikassa'),
				'type'        => 'text',
				'default'     => '',
			),
			'sandbox_signing_key'      => array(
				'title'       => __('Signing key', 'raboomnikassa'),
				'type'        => 'text',
				'default'     => '',
			),
			'webhook_details'       => array(
				'title' => __('Set the Webhook URL in the OmniKassa 2.0 dashboard to receive automatic transaction status updates.', 'raboomnikassa'),
				'type'  => 'title',
			),
			'webhook_url'               => array(
				'title'       => __('Webhook URL', 'raboomnikassa'),
				'type'        => 'text',
				'description' => __('The Webhook URL to receive automatic payment status updates ', 'raboomnikassa'),
				'default'     => add_query_arg( 'raboomnikassa2_webhook', '', home_url( '/' ) ),
				'desc_tip'    => true,
				'custom_attributes' => array('readonly'=>'readonly'),
			),
		);
	}
}