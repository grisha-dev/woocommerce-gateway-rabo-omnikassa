<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Rabo_Omnikassa2_API class.
 *
 * Communicates with Rabo Omnikassa 2.0 API.
 */
class WC_Rabo_Omnikassa2_API {
	/**
	 * URL OmniKassa API.
	 *
	 * @var string
	 */
	const URL_PRODUCTION = 'https://betalen.rabobank.nl/omnikassa-api/';

	/**
	 * URL OmniKassa sandbox API.
	 *
	 * @var string
	 */
	const URL_SANDBOX = 'https://betalen.rabobank.nl/omnikassa-api-sandbox/';

	/**
	 * Error
	 *
	 * @var WP_Error
	 */
	private $error;

	/**
	 * The URL.
	 *
	 * @var string
	 */
	private $url;

	/**
	 * Error.
	 *
	 * @return WP_Error
	 */
	public function get_error() {
		return $this->error;
	}

	/**
	 * Get the URL.
	 *
	 * @return string
	 */
	public function get_url() {
		return $this->url;
	}

	/**
	 * Set the action URL
	 *
	 * @param string $url URL.
	 */
	public function set_url( $url ) {
		$this->url = $url;
	}

	/**
	 * Get refresh token.
	 *
	 * @return string
	 */
	public function get_refresh_token() {
		return $this->refresh_token;
	}

	/**
	 * Set refresh token.
	 *
	 * @param string $refresh_token Refresh token.
	 */
	public function set_refresh_token( $refresh_token ) {
		$this->refresh_token = $refresh_token;
	}

	/**
	 * Get signing key.
	 *
	 * @return string
	 */
	public function get_signing_key() {
		return $this->signing_key;
	}

	/**
	 * Set signing key.
	 *
	 * @param string $signing_key Signing key.
	 */
	public function set_signing_key( $signing_key ) {
		$this->signing_key = $signing_key;
	}

	/**
	 * Request URL with specified method, token.
	 *
	 * @param string      $method   HTTP request method.
	 * @param string      $endpoint URL endpoint to request.
	 * @param string      $token    Authorization token.
	 * @param object|null $object   Object.
	 *
	 * @var string $url
	 * @var array $args
	 * @var WP_Error|array $response
	 * @var string $body
	 * @var mixed $data
	 * @var string $message
	 *
	 * @return object|bool
	 */
	public function request( $method, $endpoint, $token, $object = null ) {
		// URL.
		$url = $this->get_url() . $endpoint;

		// Arguments.
		$args = array(
			'method'  => $method,
			'headers' => array(
				'Authorization' => 'Bearer ' . $token,
			),
		);

		if ( null !== $object ) {
			$args['headers']['Content-Type'] = 'application/json';
			$args['body'] = wp_json_encode( $object );
		}

		// Request.
		$response = wp_remote_request( $url, $args );

		if ( is_wp_error( $response ) ) {
			$this->error = $response;

			$this->error->add( 'omnikassa_2_error', 'HTTP Request Failed' );

			return false;
		}

		// Body.
		$body = wp_remote_retrieve_body( $response );

		$data = json_decode( $body );

		if ( ! is_object( $data ) ) {
			$this->error = new WP_Error( 'omnikassa_2_error', 'Could not parse response.', $data );

			return false;
		}

		// Error.
		if ( isset( $data->errorCode ) ) {
			$message = 'Unknown error.';

			if ( isset( $data->consumerMessage ) ) {
				$message = $data->consumerMessage;
			} elseif ( isset( $data->errorMessage ) ) {
				$message = $data->errorMessage;
			}

			$this->error = new WP_Error( 'omnikassa_2_error', $message, $data );

			return false;
		}

		// Ok.
		return $data;
	}

	/**
	 * Get access token.
	 *
	 * @return object|bool
	 */
	public function get_access_token_data() {
		return $this->request( 'GET', 'gatekeeper/refresh', $this->get_refresh_token() );
	}


	/**
	 * Order announce.
	 *
	 * @param string $access_token Access token.
	 * @param array  $post_data  Data.
	 * @return object|bool
	 */
	public function order_announce( $access_token, $post_data ) {
		return $this->request('POST', 'order/server/api/order', $access_token, $post_data);
	}

	/**
	 * Get order results by the notification token.
	 *
	 * @param string $notification_token Notification token.
	 * @return object|bool
	 */
	public function get_order_results( $notification_token ) {
		return $this->request( 'GET', 'order/server/api/events/results/merchant.order.status.changed', $notification_token );
	}
}
