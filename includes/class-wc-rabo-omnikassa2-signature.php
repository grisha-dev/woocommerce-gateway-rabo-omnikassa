<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_Rabo_Omnikassa2_Signature {
	/**
	 * Calculate signature for specific data.
	 *
	 * @param array $data    Data for calculating signature
	 * @param string   $signing_key Signing Key.
	 *
	 * @var string|null $signature
	 *
	 * @return string|null
	 */
	public static function get_signature( $data, $signing_key ) {

		if ( empty( $data ) ) {
			return;
		}

		if ( empty( $signing_key ) ) {
			return;
		}

		$signature = hash_hmac(
			'sha512',
			WC_Rabo_Omnikassa2_Signature::get_signature_fields($data),
			base64_decode( $signing_key )
		);

		return $signature;
	}

	/**
	 * Validate signature.
	 *
	 * @param string $signature_a Signature A.
	 * @param string $signature_b Signature B.
	 * @return bool True if valid, false otherwise.
	 */
	public static function validate_signature( $signature_a, $signature_b ) {
		if ( empty( $signature_a ) || empty( $signature_b ) ) {
			// Empty signature string or null from calculation.
			return false;
		}

		return ( 0 === strcasecmp( $signature_a, $signature_b ) );
	}

	/**
	 * Get signature fields
	 *
	 * @param array $data    Data for calculating signature
	 *
	 * @var array $result
	 * @var mixed $v
	 *
	 * @return string
	 */
	public static function get_signature_fields($data) {
		$result = array();
		unset($data['signature']);
		array_walk_recursive($data, function($v) use (&$result) {
			if(is_bool($v)) {
				$result[] =  ($v) ? 'true' : 'false';
			}
			else {
				$result[] = $v;
			}
		});
		return implode(',', $result);
	}
}
