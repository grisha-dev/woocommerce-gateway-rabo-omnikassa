<?php
/**
 * Plugin Name: Rabo OmniKassa
 * Plugin URI: https://woocommerce.com/products/woocommerce-gateway-rabo-omnikassa/
 * Description: De Rabo OmniKassa is een uitgebreide online kassa die <strong>iDEAL</strong> en verschillende andere belangrijke Nederlandse en buitenlandse betaalmethoden ondersteunt.
 * Version: 1.1.0
 * Author: Paytium
 * Author URI: http://www.paytium.nl
 * License: GPL3
 * Requires at least: 3.8
 * Tested up to: 4.7
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '0e9973b60c742a025cb125ba64d4ee6d', '610018' );

function rabo_omnikassa_plugin_activate() {
	 empty(get_option('woocommerce_raboomnikassa_settings')) ? update_option('woocommerce_raboomnikassa_new_install', 1) : update_option('woocommerce_raboomnikassa_new_install', 0);
}
/**
 * Check if WooCommerce is active
 **/
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
	define('RABOOMNIKASSA_PATH', WP_PLUGIN_DIR . '/woocommerce-gateway-rabo-omnikassa');
	require RABOOMNIKASSA_PATH . '/class-wc-gateway-rabo-omnikassa-main.php';
	register_activation_hook(__FILE__, 'rabo_omnikassa_plugin_activate');
	new WC_Gateway_Rabo_Omnikassa_Main();
}
