<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * @package    Paytium
 *
 * @author     David de Boer <david@paytium.nl>
 * @copyright  Copyright (C) David de Boer/Paytium. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @link       http://www.paytium.nl
 * @link       http://www.davdeb.com
 */

/**
 * Paytium class.
 *
 * @package  Paytium
 * @property array main_settings
 * @property WC_Rabo_Omnikassa2_API raboomnikassa2_api
 * @since    1.0
 */
class WC_Gateway_Rabo_Omnikassa_Main {
	/**
	 * Constructor.
	 *
	 * @since   1.0
	 */
	public function __construct() {
		// Initialise the plugin
		add_action( 'init', array( $this, 'init' ) );

		// David - 2015-07-23
		// Load on every page unfortunately... Rabo OmniKassa does not accept
		// the characters needed to generate a return/notify URL that is
		// compatible with WordPress/WooCommerce API

		// Check if we need to process a payment notification
		add_action( 'wp_loaded', array( $this, 'notify' ) );
		add_action( 'wp_loaded', array( $this, 'raboomnikassa2_notify' ));
	}

	/**
	 * Initialise Paytium.
	 *
	 * @var string $url
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function init() {
		// Load the filter class
		require_once RABOOMNIKASSA_PATH . '/class-wc-gateway-rabo-omnikassa-filter.php';

		// Load the languages
		load_plugin_textdomain( 'raboomnikassa', true, '/woocommerce-gateway-rabo-omnikassa/i18n/languages' );


		require_once RABOOMNIKASSA_PATH . '/includes/class-wc-rabo-omnikassa-helper.php';
		require_once RABOOMNIKASSA_PATH . '/includes/class-wc-rabo-omnikassa2-api.php';
		require_once RABOOMNIKASSA_PATH . '/includes/class-wc-rabo-omnikassa2-signature.php';

		require_once RABOOMNIKASSA_PATH . '/class-wc-gateway-rabo-omnikassa.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-ideal.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-visa.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-mastercard.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-maestro.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-bancontact.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-vpay.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-afterpay.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-paypal.php';
		require_once RABOOMNIKASSA_PATH . '/includes/gateways/class-wc-gateway-rabo-omnikassa-cards.php';

		// Trigger WooCommerce payment gateway
		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_woocommercegateway' ) );

		$this->main_settings      = WC_Rabo_Omnikassa_Helper::get_raboomnikassa_general_options();
		$this->raboomnikassa2_api = new WC_Rabo_Omnikassa2_API();
		$url = WC_Rabo_Omnikassa2_API::URL_PRODUCTION;
		if ($this->main_settings['status'] == 'test') {
			$url = WC_Rabo_Omnikassa2_API::URL_SANDBOX;
		}
		$this->raboomnikassa2_api->set_url($url);
	}

	/** Add gateways
	 *
	 * @var array $gateways
	 *
	 * @return  array
	 */
	public function add_woocommercegateway( $methods ) {
		$methods[] = 'WC_Gateway_Rabo_Omnikassa';
		if($this->main_settings['version'] == 2){
			$gateways = array(
				 'WC_Gateway_Rabo_Omnikassa_Ideal',
				 'WC_Gateway_Rabo_Omnikassa_Visa',
				 'WC_Gateway_Rabo_Omnikassa_Mastercard',
				 'WC_Gateway_Rabo_Omnikassa_Maestro',
				 'WC_Gateway_Rabo_Omnikassa_Bancontact',
				 'WC_Gateway_Rabo_Omnikassa_Vpay',
				 'WC_Gateway_Rabo_Omnikassa_Afterpay',
				 'WC_Gateway_Rabo_Omnikassa_Paypal',
				 'WC_Gateway_Rabo_Omnikassa_Cards',
			);
			$methods = array_merge($methods, $gateways);
		}

		return $methods;
	}

	public function notify() {
		// Load the filter
		require_once RABOOMNIKASSA_PATH . '/class-wc-gateway-rabo-omnikassa-filter.php';
		$filter = new WC_Gateway_Rabo_Omnikassa_Filter;

		if ( $filter->get( 'Seal' ) ) {
			/**
			 * Return array contains various entries:
			 * - url: The URL to redirect to
			 * - message: The message to show on the redirect URL
			 * - level: The severity level of the message
			 * - status: The result of the verification OK or NOK
			 */

			// Continue processing based on bank
			$result = $this->validateRequest( $filter );

			wp_redirect( $result['url'] );
			exit;
		}
	}

	/**
	 * Verify the Omnikassa payment result.
	 *
	 * @param   WC_Gateway_Rabo_Omnikassa_Filter $filter An instance of WC_Gateway_Rabo_Omnikassa_Filter.
	 *
	 * @return  array  Array with results.
	 *
	 * @since   2.12
	 */
	private function validateRequest( WC_Gateway_Rabo_Omnikassa_Filter $filter ) {
		// Put the return data in an array, data is constructed as name=value
		$return_data = $filter->get( 'Data', '' );
		$datas       = explode( '|', $return_data );
		$data        = array();
		$return      = array( 'message' => '', 'level' => '' );

		// Convert the strings to a real array
		$orderId = false;

		foreach ( $datas as $pair ) {
			list( $name, $value ) = explode( '=', $pair );

			if ( $name == 'transactionReference' ) {
				$items = explode( 'R', $value );

				if ( isset( $items[1] ) ) {
					$orderId = $items[1];
				}
			}

			$data[ $name ] = $value;
		}

		// Can we continue processing?
		if ( $orderId ) {

			// Load the details from the database
			$transactionDetails = $this->getDetails( $orderId );

			if ( is_object( $transactionDetails ) ) {
				// Check if the result is valid
				$result = $this->checkResult( $orderId );

				// Redirect customer to final page
				if ( $result['isvalid'] ) {
					// David de Boer - 2015-07-23
					// Notify URL should never be shown to customers, even if there is no return URL
					//$redirect = ( empty( $transactionDetails->notify_url ) ) ? $transactionDetails->return_url : $transactionDetails->notify_url;
					$redirect = $transactionDetails->return_url;
				} else {
					$redirect = ( empty( $transactionDetails->cancel_url ) ) ? $transactionDetails->return_url : $transactionDetails->cancel_url;
				}

				// Create redirect URL
				if ( strstr( $redirect, '?' ) ) {
					$redirect .= '&orderid=' . $orderId;
				} else {
					$redirect .= '?orderid=' . $orderId;
				}

				$return['url'] = $redirect;
			} else {
				$return['url']     = get_site_url();
				$return['message'] = __( 'PAYTIUM_NO_LOGDETAILS_FOUND', 'raboomnikassa' );
				$return['level']   = 'error';
			}
		} else {
			$return['url']     = get_site_url();
			$return['message'] = __( 'PAYTIUM_NO_LOGID_FOUND', 'raboomnikassa' );
			$return['level']   = 'error';
		}

		return $return;
	}

	/**
	 * Check payment result.
	 *
	 * @param   int $orderId The ID of the transaction log.
	 *
	 * @return  array  Result of payment.
	 *
	 * @since   3.0
	 */
	private function checkResult( $orderId ) {
		$data            = array();
		$data['isvalid'] = true;
		$data['message'] = '';

		// Get order number from database
		$details          = $this->getDetails( $orderId );
		$data['order_id'] = $orderId;

		// Validate the status request
		$result         = $this->validateStatusRequest( $orderId );
		$transactionID  = $result['transactionID'];
		$data['status'] = $result['suggestedAction'];

		if ( ! $details ) {
			$data['isvalid'] = false;
			$data['message'] = sprintf( __( 'PAYTIUM_INVALID_TRANSACTION_NUMBER', 'raboomnikassa' ), $transactionID );

			return $data;
		} else {
			if ( ! $result['isOK'] ) {
				$data['isvalid'] = false;

				// Store the result
				if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
					update_post_meta($orderId, '_raboomnikassa_result', 'FAIL');
				} else {
					$order = new WC_Order( $orderId );
					$order->update_meta_data( '_raboomnikassa_result', 'FAIL' );
					$order->save_meta_data();
				}

				// Status Request failed
				$message = __( 'PAYTIUM_STATUS_COULD_NOT_BE_RETRIEVED', 'raboomnikassa' );
				$message .= sprintf( __( 'PAYTIUM_IDEAL_ERROR_MESSAGE', 'raboomnikassa' ), $result['error_message'] );
				$data['message'] = $message;

				return $data;
			} elseif ( ! $result['isAuthenticated'] ) {
				$data['isvalid'] = false;
				$action          = $result['suggestedAction'];

				// Store the result
				if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
					update_post_meta($orderId, '_raboomnikassa_result', $action );
				} else {
					$order = new WC_Order( $orderId );
					$order->update_meta_data( '_raboomnikassa_result', $action );
					$order->save_meta_data();
				}

				return $data;
			} else {
				$data['isvalid'] = true;

				// Store the result
				if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
					update_post_meta($orderId, '_raboomnikassa_result', 'SUCCESS' );
				} else {
					$order = new WC_Order( $orderId );
					$order->update_meta_data( '_raboomnikassa_result', 'SUCCESS' );
					$order->save_meta_data();
				}

				// David de Boer - 2015-07-23
				// Process payment to WooCommerce order
				// This is actually where the order is processed in WooCommerce!
				$order = new WC_Order( $orderId );
				$order->payment_complete();

				return $data;
			}
		}
	}

	/**
	 * Validate the status request.
	 *
	 * @param   int  $orderId   The ID of the order.
	 *
	 * @return  array  Status results.
	 *
	 * @since   3.0
	 */
	private function validateStatusRequest( $orderId ) {
		$config = get_option( 'woocommerce_raboomnikassa_settings', null );
		$filter      = new WC_Gateway_Rabo_Omnikassa_Filter;
		$return_data = $filter->get( 'Data', '' );
		$datas       = explode( '|', $return_data );
		$data        = array();
		$status      = array();

		foreach ( $datas as $pair ) {
			list( $name, $value ) = explode( '=', $pair );

			$data[ $name ] = $value;
		}

		// Set the transaction ID
		$status['transactionID'] = $data['transactionReference'];

		// Create the seal
		if ( $config['status'] == 'test' ) {
			$privatekeypass = '002020000000001_KEY1';
		}else {
			$privatekeypass = $config['privatekeypass'];
		}

		$seal = hash( 'sha256', $return_data . $privatekeypass );


		// Check the seal
		if ( $seal == $filter->get( 'Seal' ) ) {
			// Add the card status to the log
			if ( ! isset( $data['paymentMeanBrand'] ) ) {
				$brand = '';
			} else {
				$brand = $data['paymentMeanBrand'];
			}

			// Update the order data
			if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
				update_post_meta( $orderId, '_raboomnikassa_card', $brand );
			} else {
				$order = new WC_Order( $orderId );
				$order->update_meta_data( '_raboomnikassa_card', $brand );
				$order->save_meta_data();
			}

			$status['isOK'] = true;
			$status['card'] = $brand;

			switch ( $brand ) {
				case 'IDEAL':
				case 'VISA':
				case 'MASTERCARD':
				case 'MAESTRO':
				case 'MINITIX':
				case 'VPAY':
				case 'BCMC':
					switch ( $data['responseCode'] ) {
						case '00':
							$status['suggestedAction'] = 'CONFIRMED';
							$status['isAuthenticated'] = true;
							break;
						case '17':
							$status['suggestedAction'] = 'CANCELLED';
							$status['isAuthenticated'] = false;
							$status['error_message']   = __( 'PAYTIUM_RABO_OMNIKASSA_RESULT_17', 'raboomnikassa' );
							break;
						case '60':
							$status['suggestedAction'] = 'OPEN';
							$status['isAuthenticated'] = false;
							$status['error_message']   = __( 'PAYTIUM_RABO_OMNIKASSA_RESULT_60', 'raboomnikassa' );
							break;
						case '97':
							$status['suggestedAction'] = 'EXPIRED';
							$status['isAuthenticated'] = false;
							$status['error_message']   = __( 'PAYTIUM_RABO_OMNIKASSA_RESULT_97', 'raboomnikassa' );
							break;
						default:
							$status['suggestedAction'] = 'FAILURE';
							$status['isAuthenticated'] = false;
							$status['error_message']   = $data['responseCode'];
							break;
					}
					break;
				case 'INCASSO':
				case 'ACCEPTGIRO':
				case 'REMBOURS':
					$status['suggestedAction'] = $brand;
					$status['isAuthenticated'] = true;
					break;
				default:
					$status['suggestedAction'] = 'OPEN';
					$status['isAuthenticated'] = false;
					break;
			}
		} else {
			$status['isOK']            = false;
			$status['isAuthenticated'] = false;
			$status['error_message']   = __( 'PAYTIUM_SHA_VERIFY_FAILED', 'raboomnikassa' );
			$status['suggestedAction'] = 'CANCELLED';
		}

		return $status;
	}

	/**
	 * Load transaction details.
	 *
	 * @param   int $orderId The order ID to get the details for.
	 *
	 * @return        object    The transaction details
	 *
	 * @since        2.7
	 */
	public function getDetails( $orderId ) {

		if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
			$details             = new stdClass();
			$details->return_url = get_post_meta( $orderId, '_raboomnikassa_return_url', true );
			$details->cancel_url = get_post_meta( $orderId, '_raboomnikassa_cancel_url', true );
			$details->notify_url = get_post_meta( $orderId, '_raboomnikassa_notify_url', true );
			$details->trans      = get_post_meta( $orderId, '_raboomnikassa_trans', true );
			$details->result     = get_post_meta( $orderId, '_raboomnikassa_result', true );
			$details->card       = get_post_meta( $orderId, '_raboomnikassa_card', true );
		} else {
			$order = new WC_Order( $orderId );
			$details             = new stdClass();
			$details->return_url = $order->get_meta( '_raboomnikassa_return_url', true );
			$details->cancel_url = $order->get_meta( '_raboomnikassa_cancel_url', true );
			$details->notify_url = $order->get_meta( '_raboomnikassa_notify_url', true );
			$details->trans      = $order->get_meta( '_raboomnikassa_trans', true );
			$details->result     = $order->get_meta( '_raboomnikassa_result', true );
			$details->card       = $order->get_meta( '_raboomnikassa_card', true );
		}

		return $details;
	}

	/**
	 * Check if the transaction is valid.
	 *
	 * @param   string  $status  The result of the transaction.
	 *
	 * @return  bool  True if transaction is valid | False if transaction is invalid.
	 *
	 * @since   2.0
	 */
	private function isValid($status)
	{
		switch (strtolower($status))
		{
			case 'success':
			case 'authorized':
				return true;
				break;
			default:
				return false;
				break;
		}
	}

	/** Handle notification.
	 *
	 * @var object      $object
	 * @var string      $signing_key
	 * @var string|null $calculated_signature
	 * @var string      $notification_token
	 * @var object      $order_results
	 * @var bool        $more_available
	 * @var object      $order_result
	 * @var int         $order_id
	 * @var string      $status
	 * @var WC_Order    $order
	 *
	 */

	public function raboomnikassa2_notify() {
		if (!filter_has_var(INPUT_GET, 'raboomnikassa2_webhook')) {
			return;
		}
		$object = json_decode(file_get_contents('php://input'));

		$signing_key          = $this->main_settings['status'] == 'test' ? $this->main_settings['sandbox_signing_key'] : $this->main_settings['production_signing_key'];
		$calculated_signature = WC_Rabo_Omnikassa2_Signature::get_signature(json_decode(file_get_contents('php://input'), true), $signing_key);
		if (!$object || $calculated_signature !== $object->signature) return;
		$notification_token = $object->authentication;
		do {
			$order_results        = $this->raboomnikassa2_api->get_order_results($notification_token);
			$calculated_signature = WC_Rabo_Omnikassa2_Signature::get_signature(json_decode(json_encode($order_results), true), $signing_key);

			if (!$order_results || $calculated_signature !== $order_results->signature) return;
			$more_available = $order_results->moreOrderResultsAvailable;

			foreach ($order_results->orderResults as $order_result) {
				$order_id = (int)$order_result->merchantOrderId;
				$status   = $order_result->orderStatus;
				$order    = wc_get_order($order_id);
				if (strtolower($status) == 'completed') {
					$order->payment_complete($order_result->omnikassaOrderId);
				}
				else {
					$order->update_status('failed', __('Payment failed', 'raboomnikassa'));
				}
			}
		} while ($more_available);
	}
}
