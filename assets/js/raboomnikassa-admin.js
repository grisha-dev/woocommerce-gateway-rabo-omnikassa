jQuery(document).ready(function( $ ) {

    var $raboomnikassa_version = $('#woocommerce_raboomnikassa_version');

    process_raboomnikassa_version_select();

    $raboomnikassa_version.change(function () {
        process_raboomnikassa_version_select();
    });

    function process_raboomnikassa_version_select() {
        var $raboomnikassa_details_title = $('h3#woocommerce_raboomnikassa_account_details'),
        $raboomnikassa2_details_titles = $('h3#woocommerce_raboomnikassa_production_details,h3#woocommerce_raboomnikassa_sandbox_details,h3#woocommerce_raboomnikassa_webhook_details'),
        $raboomnikassa_status_section_desc = $('h3#woocommerce_raboomnikassa_status_section').next();
        if($raboomnikassa_version.val() == 1) {
            $raboomnikassa_details_title.show().next().show();
            $raboomnikassa_status_section_desc.show();
            $raboomnikassa2_details_titles.hide().next().hide();
        }
        else {
            $raboomnikassa2_details_titles.show().next().show();
            $raboomnikassa_details_title.hide().next().hide();
            $raboomnikassa_status_section_desc.hide();
        }
    }
});