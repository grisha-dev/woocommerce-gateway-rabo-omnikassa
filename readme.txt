=== Rabo OmniKassa speciaal voor WooCommerce ===
Tags: woocommerce, rabo omnikassa, omnikassa, rabobank, ideal
Requires at least: 3.8
Tested up to: 4.7
Stable tag: 1.1.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Rabo OmniKassa speciaal voor WooCommerce

== Description ==
De Rabo OmniKassa is een uitgebreide online kassa die **iDEAL** en verschillende andere belangrijke Nederlandse en buitenlandse betaalmethoden ondersteunt.
