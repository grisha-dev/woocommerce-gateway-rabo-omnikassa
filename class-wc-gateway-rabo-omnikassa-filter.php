<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * @package    Paytium
 *
 * @author     David de Boer <david@paytium.nl>
 * @copyright  Copyright (C) David de Boer/Paytium. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @link       http://www.paytium.nl
 * @link       http://www.davdeb.com
 */

/**
 * Paytium helper file.
 *
 * @package     Paytium
 * @subpackage  Helper
 * @since       1.0
 */
class WC_Gateway_Rabo_Omnikassa_Filter {
	/**
	 * Filter input.
	 *
	 * @param   string $name The name of the variable to get.
	 * @param   string $default The default value to use.
	 * @param   int $method The superglobal to get the variable from.
	 * @param   int $filter The filter to apply.
	 *
	 * @return  string  The filtered value.
	 *
	 * @since   1.0
	 */
	public function get( $name, $default = '', $method = INPUT_POST, $filter = FILTER_SANITIZE_STRING ) {
		$value = filter_input( $method, $name, $filter );

		if ( empty( $value ) ) {
			$value = $default;
		}

		return $value;
	}

	/**
	 * Get an array of filtered data.
	 *
	 * @param   string $name The name of the variable to get.
	 * @param   string $default The default value to use.
	 * @param   int $method The superglobal to get the variable from.
	 * @param   int $filter The filter to apply.
	 *
	 * @return  string  The filtered value.
	 *
	 * @since   1.0
	 */
	public function getArray( $name, $default = '', $method = INPUT_POST, $filter = FILTER_SANITIZE_STRING ) {
		$value = filter_input( $method, $name, $filter, FILTER_REQUIRE_ARRAY );

		if ( empty( $value ) ) {
			$value = $default;
		}

		return $value;
	}

	/**
	 * Filter a given value.
	 *
	 * @param   string $value The value to filter.
	 * @param   string $filter The filter to apply.
	 *
	 * @return  string  The filtered value.
	 *
	 * @since   1.0
	 */
	public function filterValue( $value, $filter = 'string' ) {
		switch ( $filter ) {
			case 'email':
				$newValue = filter_var( $value, FILTER_SANITIZE_EMAIL );
				break;
			case 'encoded':
				$newValue = filter_var( $value, FILTER_SANITIZE_ENCODED );
				break;
			case 'number_float':
				$newValue = filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT );
				break;
			case 'number_int':
				$newValue = filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
				break;
			case 'special_chars':
				$newValue = filter_var( $value, FILTER_SANITIZE_SPECIAL_CHARS );
				break;
			case 'full_special_chars':
				$newValue = filter_var( $value, FILTER_SANITIZE_FULL_SPECIAL_CHARS );
				break;
			case 'url':
				$newValue = filter_var( $value, FILTER_SANITIZE_URL );
				break;
			case 'unsafe_raw':
				$newValue = filter_var( $value, FILTER_UNSAFE_RAW );
				break;
			default:
				$newValue = filter_var( $value, FILTER_SANITIZE_STRING );
				break;
		}

		return $newValue;
	}
}
