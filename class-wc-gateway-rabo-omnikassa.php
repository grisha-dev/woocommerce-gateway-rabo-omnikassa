<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * @package    Paytium
 *
 * @author     David de Boer <david@paytium.nl>
 * @copyright  Copyright (C) David de Boer/Paytium. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @link       http://www.paytium.nl
 * @link       http://www.davdeb.com
 */

/**
 * WooCommerce payment integration.
 *
 * @package     Paytium
 * @subpackage  Woocmmerce
 * @property string instructions
 * @property string notify_url
 * @property string version
 * @property string status
 * @property string merchantid
 * @property string privatekeypass
 * @property string keyversion
 * @property string refresh_token
 * @property string access_token
 * @property string access_token_valid_until
 * @property string signing_key
 * @property WC_Rabo_Omnikassa2_API raboomnikassa2_api
 * @since       1.0
 */
class WC_Gateway_Rabo_Omnikassa extends WC_Payment_Gateway {

	public $id = 'raboomnikassa';
	public $paymentBrand = '';

	/**
	 * Constructor.
	 *
	 * @var array  $main_settings
	 * @var string $url
	 *
	 * @since   1.0
	 */
	public function __construct() {

		$this->init_form_fields();
		$this->init_settings();

		$main_settings      = WC_Rabo_Omnikassa_Helper::get_raboomnikassa_general_options();
		$this->title        = $this->get_option('title');
		$this->description  = $this->get_option('description');
		$this->instructions = $this->get_option('instructions', $this->description);

		$this->has_fields         = false;
		$this->order_button_text  = __('PAYTIUM_CONTINUE_TO_CHECKOUT', 'raboomnikassa');
		$this->method_title       = $this->id !== 'raboomnikassa' ? 'Rabo OmniKassa ' . ucfirst(substr($this->id, strpos($this->id, "_") + 1)) : 'Rabo OmniKassa';
		$this->method_description = $this->id !== 'raboomnikassa'
			? sprintf(__('All other general Rabo OmniKassa settings can be adjusted <a href="%s">here</a>.', 'raboomnikassa'), admin_url('admin.php?page=wc-settings&tab=checkout&section=raboomnikassa'))
			: __('PAYTIUM_PAYMENT_DESCRIPTION', 'raboomnikassa');
		$this->notify_url         = WC()->api_request_url(strtolower(get_class($this)));

		// Check if payment method is active
		if ( empty( $this->enabled ) ) {
			$this->enabled = 'no';
		}

		$this->version                  = $main_settings['version'];
		$this->status                   = $main_settings['status'];
		$this->merchantid               = $main_settings['merchantid'];
		$this->privatekeypass           = $main_settings['privatekeypass'];
		$this->keyversion               = $main_settings['keyversion'];
		$this->refresh_token            = $this->status == 'test' ? $main_settings['sandbox_refresh_token'] : $main_settings['production_refresh_token'];
		$this->access_token             = $main_settings['access_token'];
		$this->access_token_valid_until = $main_settings['access_token_valid_until'];
		$this->signing_key              = $this->status == 'test' ? $main_settings['sandbox_signing_key'] : $main_settings['production_signing_key'];

		if ($this->version == 2) {
			$this->raboomnikassa2_api = new WC_Rabo_Omnikassa2_API();
			$url = WC_Rabo_Omnikassa2_API::URL_PRODUCTION;
			if ($this->status == 'test') {
				$url = WC_Rabo_Omnikassa2_API::URL_SANDBOX;
			}
			$this->raboomnikassa2_api->set_url($url);
			$this->raboomnikassa2_api->set_refresh_token($this->refresh_token);
			$this->raboomnikassa2_api->set_signing_key($this->signing_key);
		}

		// Actions
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array(
			$this,
			'process_admin_options'
		) );

		// Payment listener/API hook
		add_action( 'woocommerce_api_' . strtolower( get_class( $this )), array( $this, 'processOrder' ) );

		//Add admin scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
	}

	/**
	 * Checks to see if all criteria is met before showing payment method.
	 */
	public function is_available() {
		if ($this->version == 2) {
			if (!in_array(get_woocommerce_currency(), array('EUR'))) {
				return false;
			}
		}

		return parent::is_available();
	}

	/**
	 * Add form fields for configuration in WooCommerce.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function init_form_fields() {
		$this->form_fields = WC_Rabo_Omnikassa_Helper::get_raboomnikassa_general_form_fields();
	}

	/**
	 * Required by WooCommerce to complete checkout.
	 *
	 * @param   int     $order_id The ID of the order to pay for.
	 *
	 * @var WC_Order    $order
	 * @var array       $postData
	 * @var object|bool $data
	 * @var WP_Error    $error
	 * @var object|bool $result
	 *
	 * @return  array  Redirect options.
	 *
	 * @since   1.0
	 */
	public function process_payment($order_id) {
		$order = wc_get_order($order_id);
		if ($this->version == 1) {
			// Move to the payment page
			return array(
				'result'   => 'success',
				'redirect' => $order->get_checkout_payment_url(true)
			);
		}
		try {
			$postData = array(
				'timestamp'         => date("Y-m-d\TH:i:sP"),
				'merchantOrderId'   => WC_Rabo_Omnikassa_Helper::is_pre_30() ? $order->id : $order->get_id(),
				'amount'            => array(
					'currency' => WC_Rabo_Omnikassa_Helper::is_pre_30() ? $order->get_order_currency() : $order->get_currency(),
					'amount'   => round(WC_Rabo_Omnikassa_Helper::is_pre_30() ? $order->order_total * 100 : $order->get_total() * 100),
				),
				'language'          => '',
				'description'       => '',
				'merchantReturnURL' => $this->get_return_url($order)
			);
			if (!empty($this->paymentBrand)) {
				$postData['paymentBrand']      = $this->paymentBrand;
				$postData['paymentBrandForce'] = 'FORCE_ONCE';
			}
			$postData['signature'] = WC_Rabo_Omnikassa2_Signature::get_signature($postData, $this->signing_key);
			if (!$this->is_access_token_valid()) {
				$data  = $this->raboomnikassa2_api->get_access_token_data();
				$error = $this->raboomnikassa2_api->get_error();
				if (is_wp_error($error)) {
					return;
				}
				$this->access_token             = $data->token;
				$this->access_token_valid_until = $data->validUntil;
				$this->parent_update_option('access_token', $data->token);
				$this->parent_update_option('access_token_valid_until', $data->validUntil);
			}
			$result = $this->raboomnikassa2_api->order_announce($this->access_token, $postData);
			$error  = $this->raboomnikassa2_api->get_error();
			if (is_wp_error($error)) {
				return;
			}
			if ($result) {
				return array(
					'result'   => 'success',
					'redirect' => esc_url_raw($result->redirectUrl)
				);
			}
		}
		catch (Exception $e) {
			wc_add_notice($e->getMessage(), 'error');

			return array(
				'result'   => 'fail',
				'redirect' => '',
			);
		}
	}


	/**
	 * Output for the order received page.
	 *
	 * @param   int $order_id The order ID
	 * @var WC_Order $order
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function receipt_page( $order_id ) {
		echo '<p>' . __( 'PAYTIUM_REDIRECT_TO_PAYMENT_PROVIDER', 'raboomnikassa' ) . '</p>';

		$order = new WC_Order( $order_id );

		$data                 = new stdClass;
		$data->amount         = $order->get_total();
		$data->order_id = is_callable( array( $order, 'get_id' ) ) ? $order->get_id() : $order->id;
		$data->payment_method = '';
		$data->notify_url     = $this->notify_url;
		$data->return_url     = $this->get_return_url( $order );
		$data->cancel_url     = $order->get_cancel_order_url();

		// Set the bank
		$this->bank = 'https://payment-webinit.omnikassa.rabobank.nl/paymentServlet';

		if ( $this->status == 'test' ) {
			$this->bank           = 'https://payment-webinit.simu.omnikassa.rabobank.nl/paymentServlet';
			$this->merchantid     = '002020000000001';
			$this->privatekeypass = '002020000000001_KEY1';
			$this->keyversion     = '1';
		}

		// Get the form
		$this->showForm( $data );
	}

	/**
	 * Process the WooCommerce order.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function processOrder() {

		// David - 2015-07-23
		// TODO I believe the below code is not used at all...

		$filter  = new WC_Gateway_Rabo_Omnikassa_Filter;
		$paytium = new WC_Gateway_Rabo_Omnikassa_Main;

		// Load the transaction details
		$orderId = $filter->get( 'orderid', '', INPUT_GET );
		$details = $paytium->getDetails( $orderId );

		// David 2015-07-23
		// Typo, changed $order_id to $orderID
		// Load the order
		$order = new WC_Order( $details->orderId );

		switch ( $details->result ) {
			case 'SUCCESS':
				$order->add_order_note( __( 'IPN payment completed', 'raboomnikassa' ) );
				$order->payment_complete( $orderId );
				wp_redirect( $details->return_url );
				break;
			case 'FAIL':
				$order->update_status( 'failed', sprintf( __( 'Payment %s via IPN.', 'raboomnikassa' ), strtolower( $details->status ) ) );
				wp_redirect( $details->cancel_url );
				break;
			default:
				// No action
				break;
		}
	}

	/**
	 * Show the payment form.
	 *
	 * @param   array $data The transaction data.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	private function showForm( $data ) {
		// Check if data is filled
		if ( is_object( $data ) ) {
			// Check if the amount has a maximum of 2 digits
			$data->amount = round( $data->amount, 2 );

			// Check if we have a notify URL
			if ( ! isset( $data->notify_url ) ) {
				$data->notify_url = '';
			}

			// Check if we have a cancel URL
			if ( ! isset( $data->cancel_url ) ) {
				$data->cancel_url = '';
			}

			// Check if we have a return URL
			if ( ! isset( $data->return_url ) ) {
				$data->return_url = '';
			}

			// Store the information in the log table
			if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
				update_post_meta( $data->order_id, '_raboomnikassa_return_url', $data->return_url );
				update_post_meta( $data->order_id, '_raboomnikassa_cancel_url', $data->cancel_url );
				update_post_meta( $data->order_id, '_raboomnikassa_notify_url', $data->notify_url );
			} else {
				$order = new WC_Order( $data->order_id );
				$order->update_meta_data( '_raboomnikassa_return_url', $data->return_url );
				$order->update_meta_data( '_raboomnikassa_cancel_url', $data->cancel_url );
				$order->update_meta_data( '_raboomnikassa_notify_url', $data->notify_url );
				$order->save_meta_data();
			}


			$data->pmlist = '';

			// Store the transaction reference
			$data->trans = time() . 'R' . $data->order_id;


			if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
				update_post_meta( $data->order_id, '_raboomnikassa_trans', $data->trans );
			} else {
				$order = new WC_Order( $data->order_id );
				$order->update_meta_data( '_raboomnikassa_trans', $data->trans );
				$order->save_meta_data();
			}

			// Check if all is selected
			if ( $data->pmlist == 'all' ) {
				$data->pmlist = '';
			}

			// Get WooCommerce currency and convert to Rabo OmniKassa currency code, default to EUR
            switch (get_woocommerce_currency()){
                case 'USD':
	                $woomni_currency = '840';
	                break;
                case 'CHF':
	                $woomni_currency = '756';
	                break;
	            case 'GBP':
		            $woomni_currency = '826';
		            break;
	            case 'CAD':
		            $woomni_currency = '124';
		            break;
	            case 'JPY':
		            $woomni_currency = '392';
		            break;
	            case 'AUD':
		            $woomni_currency = '036';
		            break;
	            case 'NOK':
		            $woomni_currency = '578';
		            break;
	            case 'SEK':
		            $woomni_currency = '752';
		            break;
	            case 'DKK':
		            $woomni_currency = '208';
		            break;
                default:
	                $woomni_currency = '978';
	                break;
            }

			// Collect all the fields
			$hashfields                         = array();
			$hashfields['amount']               = sprintf( "%.2f", $data->amount ) * 100;
			$hashfields['currencyCode']         = $woomni_currency;
			$hashfields['merchantId']           = $this->merchantid;
			$hashfields['normalReturnUrl']      = get_site_url() . '/index.php';
			$hashfields['automaticResponseUrl'] = get_site_url() . '/index.php';
			$hashfields['orderId']              = $data->order_id;
			$hashfields['transactionReference'] = $data->trans;
			$hashfields['keyVersion']           = $this->keyversion;
			$hashfields['PaymentMeanBrandList'] = $data->pmlist;

			// Create the data string
			$datafields = array();

			foreach ( $hashfields as $name => $value ) {
				$datafields[] = $name . '=' . $value;
			}

			// Create the seal
			$datastring = implode( '|', $datafields );
			$seal       = hash( 'sha256', $datastring . $this->privatekeypass );
			?>
			<div id="paybox">
				<form name="idealform<?php echo $data->order_id; ?>" action="<?php echo $this->bank; ?>"
				      id="idealform<?php echo $data->order_id; ?>" method="post" target="_self">
					<input type="hidden" name="Data" value="<?php echo $datastring; ?>"/>
					<input type="hidden" name="InterfaceVersion" value="HP_1.0"/>
					<input type="hidden" name="Seal" value="<?php echo $seal; ?>"/>
					<input type="submit" name="submit2"
					       value="<?php echo __( 'PAYTIUM_GO_TO_CASH_REGISTER', 'raboomnikassa' ); ?>">

				</form>
			</div>
			<?php
			$payment_info = '<script type="text/javascript">';
			$payment_info .= '	document.idealform' . $data->order_id . '.submit();';
			$payment_info .= '</script>';

			echo $payment_info;

		} else {
			echo __( 'PAYTIUM_NO_VALID_DATA', 'raboomnikassa' );
		}
	}

	public function admin_scripts() {
		wp_enqueue_style('raboomnikassa-admin-css', plugins_url( 'assets/css/raboomnikassa-admin.css', __FILE__ ));
		wp_enqueue_script( 'raboomnikassa-admin-js', plugins_url( 'assets/js/raboomnikassa-admin.js', __FILE__ ), array('jquery'), null, true );
	}

	/** Update general setting
	 *
	 * @var array $settings
	 *
	 * @return bool False if value was not updated and true if value was updated.
	 */
	public function parent_update_option( $key, $value = '' ) {
		$settings       = WC_Rabo_Omnikassa_Helper::get_raboomnikassa_general_options();
		$settings[$key] = $value;
		return update_option( 'woocommerce_raboomnikassa_settings', $settings, 'yes' );
	}

	/**
	 * Check if access token is valid.
	 *
	 * @return bool True if valid, false otherwise.
	 */
	public function is_access_token_valid() {
		if ( empty( $this->access_token ) ) {
			return false;
		}

		return strtotime( $this->access_token_valid_until ) > time();
	}
}
